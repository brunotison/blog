---
layout: page
title: La méthode
lang: fr
ref: method
permalink: /la-methode/
---

Vous pouvez appliquer la méthode Solucracy dans votre village, quartier, ville , association, entreprise pour faire remonter les besoins des citoyens et les faire passer de consommateurs à contributeurs de leur territoire

1. [Faire émerger les besoins des citoyens](../faire-emerger-les-besoins-des-citoyens)
2. [Aggréger les données](../aggreger-les-donnees)
3. [Etablir un rapport clair](../un-rapport-clair)
4. [Organiser des ateliers participatifs pour faire émerger les idées et les projets](../outils-pour-les-ateliers-participatifs)
5. [Accompagner les porteurs de projet](../accompagner-les-porteurs-de-projet)

Suite au pilote, nous avons également [documenté le coût d'une telle opération](../cout-du-projet). Cela permet d'avoir une idée des ressources à débloquer pour mener un projet Solucracy à bien.
