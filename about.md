---
layout: page
title: About
lang: en
ref: about
permalink: /about/
---

Read more about the Solucracy project and its genesis on the [collaborative space on Gitlab][gitlab].

[gitlab]: https://gitlab.com/solucracy/solucracy/blob/master/README.md