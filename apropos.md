---
layout: page
title: A propos
lang: fr
ref: about
permalink: /apropos/
---

Pour en savoir plus sur Solucracy et sa genèse, allez donc voir sur [l'espace collaboratif sur Gitlab][gitlab].

[gitlab]: https://gitlab.com/solucracy/solucracy/blob/master/README_FR.md