---
layout: post
title: "Projet pilote : Forum ouvert"
date: 2019-01-16
lang: fr
ref: projet-pilote-forum-ouvert
author: Yannick
categories: solucracy
---

Le 12 Janvier, de 14h à 18h, à la salle des fêtes Maurice Raisin de Léaz, a eu lieu le premier forum ouvert de Solucracy.

 [La méthode du forum ouvert ](https://fr.wikipedia.org/wiki/M%C3%A9thodologie_Forum_Ouvert) a été développée par Harrison Owen pour traiter des thématiques complexes en peu de temps.

24 habitants de la commune, dont plusieurs membres du conseil municipal ont participé à cet atelier pour réfléchir sur le thème : Faisons battre ensemble le coeur de notre village.

![Image](../img/leaz1.jpg)

9 bénévoles Solucracy constituaient le comité de pilotage, dont la mission était de créer un espace propice et bienveillant pour que les villageois puissent s'exprimer et réfléchir sereinement et confortablement.

![Image](../img/leaz7.jpg)
Ici, Alain, Nicolas, Paloma, Ayse, Yannick, Virginie, Laurent et Johan derrière l'appareil, guidés par Stéphan dans le processus.

Après quelques instructions, très vite, les villageois ont pu définir l'agenda de l'atelier, choisissant les sujets des groupes de discussion.

![Image](../img/leaz8.jpg)

Deux sessions de groupes de discussions ont eu lieu, répartis sur 6 espaces différents. 

![Image](../img/leaz2.jpg)

A la fin de chaque session, les initiateurs des discussions ont pu afficher un résumé de leurs conversations sur le mur des nouvelles et faire une restitution pour le groupe.

![Image](../img/leaz6.jpg)

Une fois les restitutions terminées, les habitants ont pu s'inscrire pour travailler sur les différents projets ayant émergés suite à ce travail :

*- Sensibilisation à la propreté sur le bord des routes pour que les automobilistes arrêtent de jeter leurs déchets n'importe où.*

*- Implantation d'un commerce de proximité/salon de thé/snack .*

*- Organisation d'activités pour les enfants.*

*- Création d'une micro crèche.*

*- Définition de règles d'utilisation pour le skate park, par les enfants, pour les enfants.*

*- Cantine bio sur le village pour les enfants/personnes agées/personnes en difficulté fournie par des producteurs locaux en y associant des activités collectives.*

*- Création d'un collectif de citoyens communal pour faire émerger les besoins d'intérêt général ( peut-être que ce projet vous rappelle quelque chose ? :-) ).*

*- Création d'une annexe fourrière animale pour gérer les animaux errants.*

*- Cultiver la convivialité par divers moyens : Jardin collectif, Fêtes de village, Système d'échange local, Fêtes des voisins.*

*- Un arrêt de bus pour que le bus qui passe devant le village cesse de l'ignorer.*

*- Trouver des moyens de ralentir les automobilistes qui traversent le village de Longeray.*

*- Cultiver le partage dans le village.*

*- Implémenter une monnaie locale (le Léman par exemple).*


Pour chaque projet, les habitants se sont inscrits et engagés à se réunir une nouvelle fois d'ici 15 jours.

Nous sommes également en train de chercher une date le mois prochain pour que Solucracy puisse accompagner les citoyens engagés dans la mise en place de leurs projets.

Si l'une de ces idées vous parle, que vous auriez des informations utiles à transmettre, ou que vous désirez tout simplement les aider, n'hésitez pas à nous envoyer un email à contact(AT)solucracy.com pour que nous fassions suivre  !

Merci à tous les participants de cet atelier et à l'équipe bénévole de pilotage pour avoir su les accompagner avec bienveillance dans cette aventure !

Merci également à [Stephan Krajcik](https://www.art-link.net/) pour le design de cet atelier et son aide dans l'implémentation de ce premier outil d'atelier participatif.

Dans les prochains articles, nous documenterons plus en détail la méthode utilisée, les outils, etc... pour que vous puissiez reproduire l'expérience dans votre village, quartier, ville, association, entreprise, etc.

Nous allons également lancer une journée d'atelier dans un mois ou deux pour définir le cahier des charges d'amélioration de l'application pour qu'elle vienne en soutien à la méthode décrite ici.

On vous tient au courant !



