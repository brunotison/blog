---
layout: post
title: "Forum ouvert des Colibris du Pays de Gex"
date: 2019-03-20
lang: fr
ref: forum-ouvert-colibris
author: Yannick
categories: solucracy
---

Le 3 mars 2019, dans la salle Jean Monnet à St Genis Pouilly, s'est tenu un forum ouvert co-organisé par les [colibris du Pays de gex](https://www.facebook.com/colibrisdupaysdegex) , [les Amis de la réserve naturelle de la haute chaine du Jura](http://www.arn-nature.fr/) et Solucracy.

Pour se connecter au champ des possibles et se mettre dans l'ambiance, le film [Demain](https://youtu.be/BmTySqG7yf8) a été diffusé de 10h à 12h.

Nous avons ensuite eu droit à une délicieuse [disco soupe](http://discosoupe.org/) , cuisinée par les bénévoles et approvisionnée par les invendus des magasins [Satoriz](http://www.satoriz.fr/) du Pays de Gex.

Et ensuite, forum !

Le forum était facilité par [Monica Huber](https://www.linkedin.com/in/monica-huber-fontaine-23871a42/) et [Maurizio Notarangelo](https://www.linkedin.com/in/maurizio-notarangelo-a1356a123/) qui ont su tenir le cadre et accompagner les participants tout au long de cette aventure !

Le format était un atelier sur 4h, type forum ouvert, similaire à celui décrit dans la méthode Solucracy. Merci à Monica d'avoir partagé avec nous une manière plus efficace d'afficher le déroulé :-) .

Le thème était : **Devenons acteurs de notre demain** et les participants ont été invités à répondre à la question : **Quelles solutions locales pour prendre des humaines et de la planète dans le pays de Gex ?**

Une douzaine de groupes de discussion ont eu lieu mais je vais juste vous restituer ici la liste de ceux pour lesquels des gens se sont inscrits :

+ Des cantines bio pour le Pays de Gex
+ Une discussion sur les transports en commun et la mobilité
+ La gestion des déchets
+ Les centres commerciaux qui vont se construire dans le pays de Gex (5 projets en cours apparemment)
+ La relation entre les humains et la nature
+ Les jardins partagés et la permaculture
+ La sensibilisation au tri des déchets
+ La protection de la nature et de la biodiversité
+ Un territoire 0 pesticides
+ Une maison associative et citoyenne
+ Citoyenneté : un référendum d'initiative citoyenne local
+ Une coopérative d'habitants
+ La stérilisation des chats

Si ces projets vous intéressent, n'hésitez pas à contacter les colibris du pays de Gex pour intégrer un groupe de travail.

Vous pouvez également vous rendre le **mercredi 3 Avril à 19h salle D du centre Jean Monnet à Saint Genis Pouilly**

Le financement de cette journée a permis également de tester la philosophie de [la participation libre et consciente](http://www.plumesdorees.com/web/wp-content/uploads/2016/06/Participation-libre-et-consciente.pdf), qui est un exercice à la fois pour ceux qui donnent et ceux qui recoivent.

Merci aux bénévoles qui ont donné de leur temps et de leur énergie afin de créer un espace-temps pour que les citoyens du pays de Gex puissent faire émerger des projets respectueux de l'environnement !



