---
layout: post
title: "Etablir un rapport clair"
date: 2019-01-24
lang: fr
ref: un-rapport-clair
author: Yannick
categories: solucracy
tags: method
---

Une fois les données triées, vous allez pouvoir préparer le rapport pour les habitants.

Ce rapport sera distribué à tous les habitants, il est donc important qu'il soit clair et bien structuré.
Cette rubrique va nécessiter plus d'informations mais pour l'instant voici un exemple avec le rapport créé pour le projet à Léaz(01200) en 2018 :


+ [Rapport pour les habitants](https://gitlab.com/solucracy/blog/tree/master/static/fr/rapport-leaz-2018.pdf)
