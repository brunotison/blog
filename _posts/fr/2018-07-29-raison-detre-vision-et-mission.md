---
layout: post
title: "Raison d’être, vision et mission de Solucracy"
date: 2018-07-29
lang: fr
ref: raison-detre
author: Yannick
categories: solucracy
---
A cette époque où la quête de sens est un activité prépondérante pour une grande partie d’entre nous, il est d’autant plus important que les entreprises soient capables de définir, pour elles-mêmes ou pour les gens qui les composent, la direction dans laquelle elles se dirigent et les valeurs qui leur sont propres.

C’est pour cette raison que nous avons choisi ici de décrire au mieux ce que représente le projet Solucracy.

## Raison d’être

« La raison d’être d’une organisation est de permettre à des gens ordinaires de faire des choses extraordinaires. »

*Peter Drucker*

**La raison d’être de Solucracy est de répertorier et de prioriser les problèmes rencontrés par chaque communauté pour faciliter le partage de solutions.**

Nous entendons sans cesse parler des problèmes de notre société, qui vont revenir de manière cyclique, sans qu’on aie jamais l’impression que rien n’aie avancé. Que ce soit à la télévision, lors de repas en famille ou au travail, nous revenons régulièrement dessus sans que cela ne nous mène à grand chose. Par contre, le jour où vous avez un peu de temps devant vous, il n’existe nulle part une liste des points d’amélioration, ni même des problèmes qui ont déjà été résolus.

Par où commencer ? Les solutions existent-elles déjà ? Quelle est la meilleure ?

Voilà les question auxquelles Solucracy essaie de répondre.

## Vision

“Il faut que l’idée naisse de la vision comme l’étincelle du caillou.”

*Charles-Ferdinand Ramuz*

**Utiliser les technologies actuelles pour accélérer la résolution de problèmes et focaliser l’intelligence collective pour améliorer notre société.**

Les domaines de l’intelligence collective et de la participation citoyenne évoluent rapidement et de plus en plus de techniques de concertation apparaissent pour permettre à un groupe d’humains de prendre les meilleures décisions possibles. La plupart des problèmes rencontrés à un endroit ont déjà été réglés ailleurs de manière satisfaisante, il n’est pas nécessaire de réinventer la roue, on peut adapter ce qui existe déjà.

Un seul groupe de personnes peut trouver une solution pour un problème qui impacte des dizaines de communautés aux 4 coins du monde. Jusqu’ici, le manque de visibilité était le seul obstacle.

## Mission

“Sur la terre tout à une fonction, chaque maladie une herbe pour la guérir, chaque personne une mission.”

*Sagesse Indienne*

**Fournir des outils aux citoyens pour définir le prochain défi que leur communauté locale doit relever et profiter d’une aide globale.**

Des milliers de personnes de par le monde se réunissent et relèvent les défis que posent la vie en société. Il est temps que chaque communauté puisse profiter des avancées des autres. Solucracy va mettre à portée de main tous les outils nécessaires pour passer du problème à l’implémentation de la solution le plus facilement possible.
