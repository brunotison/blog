---
layout: post
title: "Besoins, intelligence collective et efficacité"
date: 2019-02-04
lang: fr
ref: besoins-intelligence-collective-efficacite
author: Yannick
categories: solucracy
---

Cet article pose une réflexion, basée sur les différents projets auxquels je participe et les questions qu'ils soulèvent. 
Il n'est basé sur aucune expérience scientifique, uniquement sur de l'expérience personnelle et des intuitions. C'est donc mon opinion, vous aurez pleins d'occasions de ne pas être d'accord ☺ .
Mes excuses parce qu'il est un peu long.

Nous vivons une période à la fois difficile et intéressante, une période de transition, à mi-chemin entre 2 mondes. L'un qui s'éteint, pendant que l'autre émerge.

Et la rapidité à laquelle ce nouveau monde émerge va définir si notre espèce survit ou pas.
Il ne s'agit pas ici de traiter d'effondrement ou de collapsologie mais de nos capacités d'adaptation en tant qu'espèce, de notre ouverture au changement et de se demander si ce changement peut être accéléré ou s'il ne peut qu'être vécu, subi...

Les outils et méthodes de facilitation en intelligence collective sont de plus en plus nombreux et évoluent, s'affinent, s'aiguisent.

L'atelier pilote a confirmé une théorie personnelle : Il suffit que le cadre soit bien posé, que tous les participants soient au même niveau d'information et au clair sur les intentions pour que l'atelier soit fructueux.

Le niveau d'expérience des participants, leur âge etc... est secondaire à partir du moment où ils acceptent de faire confiance au processus.

Je suis sûr que beaucoup de professionnels vont me tomber dessus avec des exemples contradictoires ☺ , et j'en serais très content. Pour moi, la faille est dans la capacité des responsables du processus à tenir le cadre et dans la manière dont on s'assure que les participants l'ont bien compris et intégré (temps, méthode pédagogique, etc...).

Nous avons donc ces outils, générateurs d'idées, de projets, qui, dans le même temps, guérissent des blessures collectives et font tomber les croyances limitantes qui s'opposent à l'idée qu'un groupe d'êtres humains sélectionnés au hasard, sans formation particulière peut résoudre des problèmes complexes.

L'idée qui me plait particulièrement dans tout ça est le fait que l'on ne fasse que faciliter/canaliser quelque chose qui existe déjà. On libère la capacité des individus à s'exprimer, à communiquer et on canalise tout ça pour le focaliser sur un objectif commun.

Plutôt que la  réflexion se fasse en silo, les idées et les pensées vont se rejoindre, se confronter et se mélanger.

D'un autre côté, nous avons des besoins auxquels des individus, des collectifs, des entreprises, des institutions essaient de répondre.

Prenons par exemple l'exemple des réseaux sociaux. 

Un réseau social domine le paysage, et, à la manière d'un pays, a su se développer, faire croitre son économie et attirer rapidement de nouveaux habitants.
Ce réseau représente l'ancien monde, avec des données centralisées, un mode de gestion opaque, qui se nourrit de ses utilisateurs pour grandir.

Face à cela, nous avons une poignée d'utilisateurs qui préfèrent se diriger vers un outil plus respectueux de leurs données et de leur personne en général, à leur écoute et plus proches de leurs besoins. Ce qui me donne l'opportunité de placer [un autre projet en cours](https://www.facebook.com/groups/2290304727921896/) :-p .

Et l'on voit émerger des dizaines de projets, initiatives, idées à divers stades de maturité pour offrir des alternatives au réseau dominant.

Que ce soit une personne seule qui décide de développer elle-même toute l'application ou une communauté entière qui s'y mette, de multiples alternatives naissent et meurent, évoluent, grandissent puis s'éteignent face aux géants existants.

Les questions qui me taraudent depuis quelques mois sont les suivantes :

Est-il plus efficace de multiplier les alternatives, tester les possibilités, multiplier les expériences jusqu'à ce que l'une d'entre elle trouve le chemin ?

Ou vaut-il mieux se rassembler, s'unir et développer un outil commun pour limiter la quantité d'énergie dépensée et parvenir au but plus rapidement ?

Dans cet exemple précis : Vaut-il mieux attaquer un géant avec des millions de bactéries, ou construire un autre géant pour lutter à armes égales ?

La définition du besoin est à mon avis essentielle dans cette opération.

Chaque projet est une manière de répondre à un besoin pour soi ou un besoin que l'on a identifié chez les autres. 
La définition d'une vision pour ce projet va permettre aux individus d'y adhérer ou non en répondant à diverses questions :

_Est-ce qu'en participant à ce projet, je vais remplir mon besoin ?_

_Est-ce que ce projet est la meilleure manière de remplir mon besoin ?_

_Est-ce que le besoin que j'ai est assez important par rapport à mes autres besoins et à mes ressources pour que je dépense de l'énergie dans ce projet ?_

La psychologie sociale nous a bien sûr montré qu'il y a encore des dizaines de questions que l'on peut se poser et que notre méthode de réflexion est loin d'être rationnelle ( voici d'ailleurs un [site très intéressant](http://changingminds.org/) qui liste tout ça si vous comprenez l'anglais).

Mais l'idée est donc qu'une proposition X va émerger, une ou plusieurs personnes vont s'atteler à faire vivre et grandir cette proposition en dépensant de l'énergie et des ressources.

Et soudain, une proposition Y va émerger, à quelques kilomètres de là, en suivant le même processus. 

Si on fait entrer en communication les géniteurs des propositions X et Y , les obstacles à leur fusion sont multiples.

Peut-être que la vision varie légèrement, peut-être que le stade de maturité est différent, peut-être que l'ego des géniteurs les empêchent de laisser mourir leur proposition pour rejoindre l'autre, peut-être que la quantité d'énergie déjà investie est trop importante, peut-être que le système les met en compétition, etc...

Et donc, on se retrouve avec 2 groupes de personnes qui dupliquent le travail et l'énergie dépensée juste parce que 2% de leurs projets respectifs diffèrent ou juste qu'ils ont cette impression.

Et c'est là qu'une phrase de Lionel Lourdin me revient, tel un bon vieux flashback à la Star Wars ☺ : **Il est important de dissocier l'intention du projet.**

Si le projet coule, l'intention reste intacte, et on peut continuer à avancer. Ca rend également la fusion beaucoup plus facile.

S'ils travaillaient ensemble sur un socle commun à 98% et se dissociaient pour 2%, est-ce que ça ne serait pas plus efficace ?

Pour moi, la différence entre l'ancien monde et le nouveau monde est là.

On passe d'un fonctionnement en silos à des vases communicants, d'une approche globale à une approche modulaire.
On capitalise sur ce qui existe et on fait uniquement avancer ce qui manque.

Voici quelques métaphores qui permettront peut-être de mieux comprendre la quantité d'énergie et de ressources qui est économisée en adoptant cette logique :

* 25 voitures allant d'un point A à un point B / un bus.
* Acheter une visseuse et s'en servir 10 minutes par an / avoir une visseuse en commun pour tout l'immeuble.
* Tirer le cablage depuis la centrale électrique jusqu'à sa maison / utiliser le réseau existant depuis la maison du voisin.
* Laisser chaque membre de la famille se faire à manger dans son coin tour à tour / préparer un repas commun.
* 10 constructeurs automobiles qui dépensent des millions pour développer leur moteur et le faire breveter / des centaines d'ingénieurs qui travaillent sur un projet open source de moteur propre.

Cela soulève bien sûr des questions : 

* Et si le socle est pourri ? Ne vaut-il pas mieux repartir de 0 ?
* Comment savoir ce qui existe déjà ?
* Comment faire pour que les personnes qui portent des projets adoptent cette démarche ?

Ces questions sont au coeur du projet Solucracy à bien des niveaux :

1. Le 2 mars, nous allons nous réunir pour définir un cahier des charges pour l'application qui viendra en soutien à la méthode. D'ailleurs vous pouvez vous inscrire [ici](https://www.eventbrite.fr/e/inscription-solucracy-un-tiers-lieu-pour-la-feuille-de-route-55146059348) 
(je me trouve très fort en placement de produit ☺)
A l'issue de cet atelier, l'intention est d'abandonner mon code tout pourri que j'ai passé 4 ans à écrire pour repartir d'un socle sain de logiciel libre et créer des modules ré-utilisables ou ré-utiliser des modules existants.

1. Les projets qui émergent lors des ateliers Solucracy sont parfois novateurs, parfois similaires à d'autres initiatives et les citoyens seraient sûrement très contents de n'avoir à s'occuper que des 2% qui font leur différence.

1. La mise en compétition, la polarisation, les divisions, la mise en silo sont peut-être des outils qui ont fonctionné par le passé mais ils n'ont plus leur place si on veut avoir un futur.

Si vous êtes arrivés jusqu'ici, je vous remercie d'avoir lu tout ça et j'espère que ça a pu vous aider à avancer dans votre propre réflexion, ce qui voudrait dire que j'ai participé à construire les 98% ☺
 
PS : En relisant, j'ai la réponse à ma question, il faut créer un géant avec des bactéries....

