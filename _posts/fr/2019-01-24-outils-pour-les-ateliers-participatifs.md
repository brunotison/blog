---
layout: post
title: "Outils pour les ateliers participatifs"
date: 2019-01-24
lang: fr
ref: outils-pour-les-ateliers-participatifs
author: Yannick
categories: solucracy
tags: method
---

L'objectif des ateliers participatifs est d'amener les participants à proposer des projets ou des idées qui permettront d'améliorer la qualité de vie de leur territoire.

Il existe plusieurs manières d'accomplir cet objectif et nous allons essayer de les lister et les documenter ici pour qu'elles puissent être facilement mises en place.
Afin qu'elles s'intègrent à la méthode Solucracy, il est nécessaire qu'à la fin de l'atelier, les participants aient pu proposer des projets et s'inscrire en tant que porteurs de ces projets.

*  [Forum ouvert](../le-forum-ouvert)

A la fin de chaque atelier, vous devez obtenir une liste de projets pour chaque projet, une liste de participants pour pouvoir embrayer sur l'étape suivante : [Accompagner les porteurs de projet](../accompagner-les-porteurs-de-projet)

L'idéal serait bien sûr de terminer sur un repas dansant en laissant au mur la fresque pour que tous les convives puissent admirer les résultats de l'atelier et s'inscrire sur les projets qui les inspirent.
