---
layout: post
title: "Les gilets jaunes, une perte nette d'énergie ?"
date: 2018-11-19
lang: fr
ref: les-gilets-jaunes-une-perte-nette-denergie
author: Yannick
categories: solucracy
---

#### Ou comment récupérer vilement le mouvement des gilets jaunes pour promouvoir Solucracy :-)

Le 17 Novembre a eu lieu l'opération gilets jaunes et elle continue probablement un peu aujourd'hui.

Des dizaines de milliers de citoyens ont fait don de leur temps, une journée entière, voire un weekend pour beaucoup, pour descendre dans la rue et exprimer un besoin qui, selon eux, n'était pas entendu ou écouté par le gouvernement.

Pour faire entendre ce besoin, ils ont bloqué les routes et empêché d'autres citoyens de vaquer à leur occupation.

Pour faire entendre ce besoin, une personne est morte, plusieurs centaines ont été blessées, et les tensions entre le gouvernement et les citoyens sont encore plus fortes que jamais.

Le nombre de personnes exprimant ce besoin n'est pas clair. La définition du besoin lui même n'est pas claire.

Les personnes dans la rue souhaitent-elles que le carburant soit moins cher ? Que leurs salaires soient plus hauts ? Que les transports en commun soient plus développés ? Gratuits ?

Par le passé, on a vu que lorsque ce genre d'incident arrive, ce sont les médias qui vont interpréter la situation et la retransmettre à leur manière.

Donc on a : plusieurs centaines de milliers d'heures, de litres de carburant, d'euros de dégât et de salaires de CRS qui ont été dépensés pour faire entendre un besoin qui ne sera probablement pas entendu correctement et oublié d'ici environ 2 semaines, le temps que l'info soit noyée par un autre événement.

#### Pourquoi Solucracy ?

Imaginons maintenant que les personnes concernées, avant de descendre dans la rue, renseignent leurs besoins sur Solucracy. Mettons par exemple que le problème soit : **Les prix des carburants sont trop hauts.**

Mettons que la première personne qui renseigne le problème signale que le besoin sous jacent est : **J'ai besoin de faire 30 kms par jour pour aller travailler et là ça me revient trop cher**.

Puis tour à tour, chacun va voter pour le problème et expliquer quel est son besoin. A terme, nous obtiendrons donc un problème avec 200000 votes, leur répartition sur le sol français et les besoins clairement exprimés. Par exemple, 40% des votants ne peuvent plus financer le trajet vers leur lieu de travail tandis que 20% sont des entrepreneurs qui ne peuvent faire tourner leur entreprise.

Tout ça a coûté quelques minutes à chacun, vous avez un problème et des besoins clairement définis et le plus important : personne ne peut nier ce besoin. Il ne sera pas interprété par des journalistes. Il ne sera pas oublié après 2 semaines. Il sera posé là et documenté.

Et parmi toutes les personnes qui sont descendues dans la rue, il y a forcément des ingénieurs, des économistes, des routiers, des experts dans tous les domaines. Ces personnes peuvent donc également proposer des solutions et réfléchir à la meilleure manière de remplir ce besoin.

Certes, peut-être que la meilleure solution est de diminuer le coût du carburant ? Quels sont les avantages ? Quels sont les inconvénients ? Est-ce que nous sommes bien sûrs que ça résout le problème pour tout le monde ?

Si ce n'est pas le cas, Solucracy permettra de le savoir, et de savoir pourquoi. Et de faire d'autres propositions.

#### Mais a quoi ça sert ?

Une  fois que le problème est clairement défini, que les besoins sont clairement définis et que l'on a un processus pour proposer des solutions, **n'importe qui peut proposer des solutions.**

Si le gouvernement choisi de faire la sourde oreille, peut-être qu'une association, une entreprise, un parti politique adverse va décider de mettre en oeuvre les solutions proposées. Peut-être que vous pourrez résoudre le problème avec vos voisins, pour votre quartier.

Est-ce que ça ne vaut pas la peine d'essayer ? Est-ce qu'on a tant à perdre que ça à essayer des manières différentes de résoudre nos problèmes ?
