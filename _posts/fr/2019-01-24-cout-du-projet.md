---
layout: post
title: "Coût du projet"
date: 2019-01-24
lang: fr
ref: cout-du-projet
author: Yannick
categories: solucracy
tags: method
---

Les chiffres ci-dessous peuvent varier en fonction du contexte et de la région dans laquelle vous vivez. Nous avons fait ça à partir du projet déployé dans un village de 700 habitants (380 boites aux lettres) et pour un atelier participatif ou nous attendions 50 personnes.

Tout est calculé pour que ça coûte le moins cher possible. Il est possible par exemple d'imprimer des affiches plutôt que de les faire à la main, mais ça a un coût.
Les temps de réunion de préparation de tout ça et les trajets pour se rendre sur site ne sont pas compris.

Nous partons sur un coût d'impression de feuille noir et blanc à 0,08 euros la feuille.


## Collecte d'infos

**Le prix de revient par boîte aux lettres est de : 0,24 euros**

Cela comprend le flyer et le rapport avec les résultats.
Cela ne comprend pas un stylo et un cahier pour chaque personne qui va collecter les infos.

**Le temps nécessaire par boîte aux lettres est d'environ 7 minutes.**

Cela comprend la distribution des flyers, le porte à porte pour aggréger les réponses et la distribution des rapports complets.

*Exemple : si je veux lancer une collecte d'infos sur une population de 100 boites aux lettres, cela me coutera 24 euros et 11,6 heures, si je suis déjà sur place et que je fais tout ça à pied.*

## Aggréger les données et établir un rapport clair

**Il faut compter 3 minutes par boite aux lettres pour enregistrer les données.**

**Il faut compter 4 heures de travail pour créer le rapport.**

## Forum ouvert

Pour le forum ouvert, nous calculons par tranche de 50 personnes.

**Le prix de revient sera d'environ 300-350 euros.**

Cela comprend les fournitures (scotch, feutres, feuilles A2, A0, post-its, l'impression des dossiers participants).
Cela ne comprend pas le repas du midi, l'apéro du soir (qui peuvent être en mode buffet canadien), la prestation d'un facilitateur et la location de la salle. 

**Le temps total nécessaire sera d'environ 70 heures.**

Ce temps comprend l'atelier de 4h pour 9 personnes, 2h de briefing pour le comité de pilotage, la préparation des dossiers participants, 30mn de pause repas le midi et la préparation et le rangement de la salle.
Ce temps ne comprend pas l'apéro du soir ☺ .

*Nous allons également développer un petit outil pour calculer automatiquement une estimation du coût en fonction du nombre de boites aux lettres et du nombre de bénévoles. Ca permettra d'avoir une idée plus rapide et plus facilement.
Merci de nous faire signe si vous voulez essayer de le développer, on peut vous fournir les données ☺*

