---
layout: post
title: "Remerciements"
date: 2018-08-27
lang: fr
ref: remerciements
author: Yannick
categories: solucracy
---

Solucracy est un gros projet qui a nécessité des centaines d'heures de travail, d'erreurs, d'arrachage de cheveux et de tir de balles dans les pied.

Il n'aurait pu voir le jour une vingtaine d'années auparavant tout simplement parce que la communauté open source n'était pas aussi développée qu'elle l'est maintenant.

C'est donc à mon avis tout à fait naturel de remercier toutes les personnes, communautés et organisations qui ont participé ou facilité la mise en place de Solucracy.

##### Personnes

- Valentin Coudert a développé l'interface de la toute première version et créé l'intégration avec Google maps.
- [David Sanders](https://www.facebook.com/Ynkaba-286716328056727/) a fait le design des logos de catégories et d'une bonne partie des pages de l'application. Il a également créé le logo à l'époque où le site s'appelait Democracia.fr
- Gregory Genlot a testé la première version de Democracia.Fr et fourni le rapport de bug le plus professionnel que j'ai vu jusqu'à maintenant. Et il y en avait un paquet :-)
- Lorenzo Favia pour la mise en contact avec tout un tas de gens magnifiques qui se battent constamment pour faire avancer notre société

##### Organisations

- [Démocratie ouverte](https://democratieouverte.org/), qui oeuvre pour mettre en réseau les acteurs de l'innovation démocratique et du développement de la participation citoyenne.
- [Bio-optronics](https://bio-optronics.com/?lang=fr) , mon précédent employeur, pour m'avoir fait découvrir le monde du logiciel et donné le temps de rassembler mes forces pour me lancer.
- L'association [Pangloss Labs](http://panglosslabs.org/) pour m'avoir accompagné, conseillé, soutenu, guidé et pour continuer à le faire.
- [Les Crapauds fous](https://crapaud-fou.org/) et particulièrement [Mose](http://mose.com/index-fr.html) pour leur soutien et leur aide pour libérer le code, faire grandir le projet et le rendre disponible à tout le monde !

##### Logiciels Gratuits et Open source

Comme je disais, tout cela n'aurait pas été possible sans les logiciels et outils libres et gratuits sur lesquels travaillent des milliers de personnes chaque jour :

- [Wamp](http://www.wampserver.com/), plate-forme de developpement web qui permet d'installer facilement un environnement PHP/Mysql pour créer des sites internet.
- [Stack Overflow,](https://stackoverflow.com/) source de connaissances et d'inspiration incroyable. Sans ce site, chaque bug, chaque question de programmation aurait pris 10 fois plus de temps à trouver sa solution.
- [Font Awesome,](https://fontawesome.com/) qui permet gratuitement d'utiliser ses icônes sous forme de police de caractères, et évite des heures de graphisme.
- [Bootstrap,](https://getbootstrap.com/) boîte à outils de développement d'interfaces, qui permet de créer rapidement et facilement des pages adaptées à tous les formats d'écran.
- [JQuery,](https://jquery.com/) librairie de fonctions JS. Autant de raccourcis qui permettent d'ajouter des fonctionnalités magnifiques sans avoir à passer des heures à débugger du code.
- [EverSql](https://www.eversql.com/sql-query-optimizer/), optimiseur de requêtes SQL, grâce à qui la liste des problèmes ne met pas 83 secondes à charger...
- [Google](https://www.google.com/), sans qui le coût de tout ce projet aurait été largement supérieur, et même s'il n'est pas exclu de passer sur Open Street Map , Google Suite sera toujours utile pour s'organiser.
- [Tarte au Citron](https://opt-out.ferank.eu/fr/), pour le nom original et l'économie d'heures de dev pour se mettre en conformité avec la RGPD.
- [Jquery TagsInput](http://xoxco.com/projects/code/tagsinput/), pour leur petit interface de saisie de mots clés.
- [Stripe](https://stripe.com/fr), dont l'interface très simple à mettre en place vous permet de faire des dons de manière sécurisée
- [JQuery Growl](https://github.com/ksylvest/jquery-growl), qui permet d'afficher les badges que vous gagnez au fur et à mesure.
- [Github](https://github.com/), qui permet de stocker tout le code et de s'assurer qu'il n'y aie pas besoin de m'amputer d'une jambe quand je me tire une balle dans le pied.
- [SwiftMailer](https://swiftmailer.symfony.com/), qui vous envoie vos emails.
- [Inkscape,](https://inkscape.org/fr/) qui permet de faire du dessin vectoriel, de créer des icônes etc... si c'est moche, ce n'est pas de la faute d'Inkscape mais celle de mes goûts.

La liste va sans aucun doute encore s'allonger. Si, vous aussi, désirez participer à cette aventure, n'hésitez pas à nous contacter !

Le code est [ici](https://gitlab.com/solucracy) 
