---
layout: post
title: "Aggréger les données"
date: 2019-01-24
lang: fr
ref: aggreger-les-donnees
author: Yannick
categories: solucracy
tags: method
---

Une fois les données collectées, vous et votre équipe aurez déjà une bonne vision d'ensemble des problématiques du territoire. Vous saurez comment les habitants voient leur quartier, leur village, etc... et pourrez identifier les demandes qui reviennent le plus souvent.

Il est important que les citoyens puissent profiter également de cette vision d'ensemble et un rapport un peu condensé rendra l'expérience plus agréable.
L'application Solucracy permettra de vous assister dans ce processus, mais pour l'instant, malheureusement, c'est du manuel ! ☺

**1. Numériser les données.**

Le plus simple est pour l'instant de saisir les données dans un tableur. Vous pourrez en trouver un avec les colonnes toutes prêtes [ici](https://gitlab.com/solucracy/blog/tree/master/static/fr/modele-collecte-donnees.ods)

**2. Catégoriser**

Chaque personne aura peut-être répondu plusieurs choses à chaque question. Il va donc falloir séparer tout ça pour pouvoir les ranger correctement et établir des statistiques.
Par exemple, pour la question : Que souhaiteriez-vous avoir de plus pour améliorer la qualité de vie ?

La personne A a répondu : plus de transports publics, améliorer la sécurité du carrefour rue Machin, éteindre l'éclairage public la nuit.
La personne B a répondu  : Une cantine bio, améliorer la sécurité du carrefour rue Machin.

Vous allez donc avoir les lignes suivantes :

_plus de transports publics

_améliorer la sécurité du carrefour rue Machin

_améliorer la sécurité du carrefour rue Machin

_éteindre l'éclairage public la nuit

_Une cantine bio

Pour chaque ligne, vous pourrez ensuite définir des catégories comme (Sécurité routière, Economie d'énergie, mobilité, cantine) et les assigner.

Si vous comptez ensuite le nombre de fois où chaque catégorie apparait, vous obtiendrez des statistiques un peu plus faciles à lire.

A titre d'exemple, voici les documents générés lors du projet pilote à Léaz :

+ [Données collectées](https://gitlab.com/solucracy/blog/tree/master/static/fr/donnees-collectees-leaz-2018.ods)
