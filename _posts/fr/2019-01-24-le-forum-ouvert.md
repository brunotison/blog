---
layout: post
title: "Le Forum Ouvert"
date: 2019-01-24
lang: fr
ref: le-forum-ouvert
author: Yannick
categories: solucracy
tags: method
---
(ce document est encore en construction, pardon pour la poussière)

## Qu'est ce que le forum ouvert ?

[Vous pourrez trouver ici toutes les informations disponibles sur le sujet](http://www.dianegibeault.com/OS_Intro_F.htm)
Le concept du forum ouvert peut paraitre simple au premier abord mais il est recommandé de faire appel à un professionnel facilitateur en intelligence collective pour animer l'atelier. Nous allons documenter une liste de personnes à même de vous accompagner dans ces opérations.

## Comment l'appliquer à Solucracy ?

*  ### L'équipe d'encadrement

1.  L'hôte(sse)

La personne représentant l'entité (association, collectivité, entreprise) à l'origine de l'initiative. Ses objectifs seront, entre autres, de donner le ton de la rencontre, de centrer l'attention sur le thème et les objectifs du Forum Ouvert et de stimuler les participants

1.  Le facilitateur/la facilitatrice

Le facilitateur est le gardien du processus, qui va accompagner les participants tout au long de l'atelier et s'assurer que le cadre soit clair et présent.

1.  Le/la responsable logistique

Le responsable logistique va gérer les détails qui passent inaperçus quand tout va bien mais peuvent ruiner l'après midi si quelque chose manque : Que le bar soit fourni, que l'accueil des participants se déroule correctement, qu'il ne manque pas de stylos, de papier, etc...

2.  Le/la preneur/preneuse de notes

Le preneur de notes s'assure que tout le travail des participants sera bien documenté et disponible pour plus tard, que les projets seront renseignés et les informations de contacts prises

3.  Le barman / La barmaid

Le barman va s'assurer que tout le monde soit bien hydraté et doit être disponible tout au long de l'atelier pour servir les cafés, thés, etc... Il est très important de ne pas servir d'alcool durant l'atelier.

*  ### Le matériel

Il vous faudra des stylos, des feutres, un rouleau de papier A0 pour la fresque finale, du scotch papier et des feuilles A2 pour chaque groupe de discussion. Toute initiative, idée ou technique pour diminuer la quantité de matériel nécessaire est bienvenue :-)

*  ### Les affiches

*  ### Le déroulé

Horaires Contenu en pensant que le FO démarre à 14h00
 
<style type="text/css">
    table.tableizer-table {
        font-size: 12px;
        border: 1px solid #CCC; 
        font-family: Arial, Helvetica, sans-serif;
    } 
    .tableizer-table td {
        padding: 4px;
        margin: 3px;
        border: 1px solid #CCC;
    }
    .tableizer-table th {
        background-color: #104E8B; 
        color: #FFF;
        font-weight: bold;
    }
</style>
<table class="tableizer-table">
<thead><tr class="tableizer-firstrow"><th></th><th>hôte :  connaît le contexte, fait les interventions spécifiques. Suivra le processus dans le temps et oriente avec son discours d'ouverture la journée. Donne la perspective possible  des travaux sur plusieurs mois ou années</th><th>facilitation connaît le processus participatif de la journée tient le cadre, ajuste le cadre, est attentif a l'objectif du jour. </th><th>assistant-es : oriente, accueille, aide les participants à afficher, clarifie rassure aide à compléter les fiches, mettre le nom.</th><th>participant- choisissent les thèmes, constituent les groupes,  ont les idées, dégagent des priorités , sont les clés de d'action sur le terrain</th><th>actions</th><th>matériel</th></tr></thead><tbody>
 <tr><td>Préparation</td><td>le discours qui place le contexte</td><td>organise l'espace d'accueil - règles du forum ouvert dans son ensemble - règles dans les sous-groupes - déroulement du processus</td><td>se placent vers le tableau</td><td>lise leur page reçue à l'accueil.</td><td>- installer la salle en cercle, sur 1 ou 2 rang, - installer les chaises des sous-groupes. - titre de l'événement et question du jour</td><td>- Préparer 2 feuilles flip chart pour mettre les ateliers de la journée classés de 1 à  10 et numéro des tables - micro - feutre scotch grande feuilles, post it -feuilles A4 panneau d'affichage (marché)</td></tr>
 <tr><td>14.00-14h15</td><td>&nbsp;</td><td>Installation du cercle d'ouverture. Mot de Bienvenue par l'hôte. </td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>14.15- 14.30h.</td><td>&nbsp;</td><td>Présentation du processus du forum ouvert * énoncer et clarifier les règles affichées.</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>grande feuille A4, stylo au centre</td></tr>
 <tr><td>14.30-15.00</td><td>&nbsp;</td><td>Constitution de l'agenda / Propositions de thèmes (thème+ nom) :,  regroupage des thèmes similaires/réglage</td><td>les assistant-es guident les personnes avec les thèmes à afficher,  </td><td>Micro-Tour à tour les personnes se lèvent écrivent leur thème et l'énonce</td><td>Affichage sur la place du marché</td><td>Un panneau pour affichage libre. Grande feuille avec un tableau à double entrée avec les 10 tables et  les deux sessions.</td></tr>
 <tr><td>15.00-15.45h</td><td>participe</td><td>sous-cercle par thème 1ère session 10' avant la fin indique le temps pour préparer le compte rendu qui se fera à la fin.</td><td>participent</td><td>dans le groupe, animateur, secrétaire, bienveillance reconnaître la diversité des point de vue, chercher la convergence</td><td>&nbsp;</td><td>préparer feuille A2, stylo par groupe et 3 post-its pour dégager 3 points à présenter lors de la restitution</td></tr>
 <tr><td>15.45-16.00h</td><td>&nbsp;</td><td>tite pause</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>café, thé, poubelle</td></tr>
 <tr><td>16.00-16.45h</td><td>&nbsp;</td><td>2ème, session 10' avant la fin indique le temps pour préparer le compte rendu qui se fera à la fin</td><td>&nbsp;</td><td>&nbsp;</td><td>Afficher les comptes rendus par table</td><td>&nbsp;</td></tr>
 <tr><td>16.45 17.15h</td><td>&nbsp;</td><td>restitution :timing des comptes rendus des groupes en 3 points, 1-2 min. et</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
 <tr><td>17.15-17.45h</td><td>&nbsp;</td><td>Invite les participants à rejoindre les groupes d'action qui les intéressent. une /deux prochaine date sont fixées</td><td>&nbsp;</td><td>&nbsp;</td><td>Inscription  sur les feuilles participants contributeurs</td><td>&nbsp;</td></tr>
 <tr><td>17.45-18.00</td><td>Mot de fin du forum, remerciements, et prochaines rencontre prévue et à prévoir, aide au rangement ?</td><td>Cercle de clôture </td><td>&nbsp;</td><td>selon le nombre de participants-e  1 ou 3-4 mots, </td><td>&nbsp;</td><td>en cercle faire tourner le micro</td></tr>
</tbody></table>


Merci à [Monica Huber]( https://www.linkedin.com/in/monica-huber-fontaine-23871a42/) pour la manière d'afficher le déroulé ! Cela rend beaucoup plus clair le positionnement de chaque acteur pour chaque étape.
