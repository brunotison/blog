---
layout: post
title: "Faire émerger les besoins des citoyens"
date: 2019-01-24
lang: fr
ref: faire-emerger-les-besoins-des-citoyens
author: Yannick
categories: solucracy
tags: method
---

L’objectif de cette première étape est de cartographier les besoins et problèmes des usagers du territoire.

  

Tous les citoyens doivent avoir les mêmes opportunités de s’exprimer, c’est pour cette raison que plusieurs outils sont décrits ici en plus de l’outil numérique.

  

La dépose dans les boites aux lettres d’un petit flyer qui décrit la démarche, annonce les ateliers participatifs, liste les questions qui seront posées et donne un lien vers les formulaires web améliorera la qualité de l’accueil par les citoyens, particulièrement si vous portez un pull ou un T-shirt de la couleur du flyer pendant l’opération.

Vous trouverez un modèle de flyer éditable, prêt à envoyer à l’imprimeur [ici](https://gitlab.com/solucracy/blog/tree/master/static/fr/modele-flyer-bal.svg)

  

Lorsque vous vous préparez à interroger quelqu’un, il est utile de rappeler la démarche en quelques phrases :

  
*Bonjour, je suis XXXX de Solucracy. Il s’agit d’une initiative citoyenne pour faire émerger les besoins des habitants de XXXXX . Ensuite, nous allons les agréger et organiser un ou plusieurs ateliers participatifs pour faire émerger des projets et des idées. Auriez-vous 2 minutes à m’accorder pour répondre à 4 questions ?*

  

Les questions posées sont les suivantes :

  

__1. Qu'est-ce qui vous plaît dans l’endroit où vous vivez actuellement ?__


Cette question permet de repérer ce qui fonctionne, de faire un état des ressources du territoire

  

__2. A quelles activités aimez-vous participer ?__
  

Nous recensons ainsi les différents lieux créateurs de lien, noeuds du tissu social

  

__3. Quelles activités supplémentaires souhaiteriez vous ?__
  

Permet à la fois de savoir ce qui permettrait de relier les gens et de savoir si la communication par rapport aux activités existantes est efficace

  

__4. Que souhaiteriez-vous avoir de plus pour améliorer la qualité de vie ?__
  

Cette question permet d’aborder les problèmes et besoins selon une perspective constructive.

  

Il arrive parfois qu’une personne donne beaucoup plus d’informations que les simples réponses aux questions. N’hésitez pas à les noter à part. Elles pourront être triées après et fournir des détails précieux sur le territoire.

  

## Outils de collecte des données :

  

- Porte à porte 
  

Si vous disposez de peu de volontaires, pour optimiser, le porte à porte est plus efficace le mercredi ( quand les enfants ne sont pas à l’école ) et le samedi. Par principe, nous avons évité le dimanche.

Suivant la démographie et les habitudes des habitants, un démarchage en soirée peut fonctionner mais s’il fait nuit, qu’il pleut ou qu’il fait trop froid ou si vous tombez pendant le repas, les gens ouvriront rarement, ou ne seront pas pleinement disponibles pour vous répondre.

  

- Permanences 
  

Si les moyens, le climat et les lieux le permettent, vous pouvez également mettre en place une permanence pour que les gens puissent venir vous voir et répondre aux questions. 

L’endroit propice sera de préférence :

_Dans une zone piétonne

_Un lieu de passage

_Visible

  

- Boîtes aux lettres 
  

Il est également possible de fabriquer de petites boites aux lettres dont vous trouverez les plans ici.

Si vous les alimentez en formulaires et y attachez un stylo ( qu’il faudra probablement changer régulièrement), il suffira de récolter le contenu de temps en temps. 

Elles peuvent être utiles à l’entrée d’un lotissement, ou près d’un bloc de boites aux lettres.

  

- Micro trottoirs 
  

Il est important d’identifier les points névralgiques du territoire pour effectuer ces micro trottoirs : Sortie des écoles, commerces, restaurants, bars, Entreprises/sortie d’usine ou des points ponctuels comme un événement particulier (loto, fête des voisins, etc…)

  

- Formulaire web 
  

Il est possible d’utiliser un formulaire web comme [Framaform ](https://framaforms.org/)en attendant que l’application Solucracy soit prête.
