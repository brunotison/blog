---
layout: post
title: "L'énergie vient d'en bas"
date: 2019-02-18
lang: fr
ref: lenergie-vient-den-bas
author: Yannick
categories: solucracy
---

### Les ressources humaines

La question de l'approvisionnement en énergie est de plus en plus présente dans les conversations, qu'on parle de pic pétrolier, d'énergies renouvelables, d'économie, de stockage. On parle de pétrole, de solaire, d'éoliennes, etc...
Et les producteurs majeurs d'énergie se reconvertissent peu à peu pour laisser place à la distribution. D'une source centralisée, on passe à de [multiples sources](https://www.edf.fr/energie-locale).

La manière la plus efficace de stocker l'énergie (le pétrole) étant en train de disparaitre (ou du moins de cesser d'être économiquement intéressant), on s'oriente vers des technologies qui permettent de la répartir localement le plus efficacement possible en minimisant les besoins de stockage

J'aimerais argumenter qu'une transformation similaire est en train de se produire au niveau social ( même si [Marc Halevy](https://youtu.be/zV61ytSgQxI) l'a fait bien avant moi de manière plus efficace et intelligente).

La ressource humaine, bien que je trouve le terme trop mécanique, provient, avant tout, de l'humain.

La plupart des structures actuelles (entreprises,institutions) fonctionnent de manière pyramidale et consistent à s'organiser pour qu'une personne censée posséder toutes les informations nécessaires dirige une équipe d'exécutants. 
L'optimisation de la structure consistera à essayer de s'arranger pour que les ordres qui arrivent de la strate supérieure puissent redescendre de la manière la plus claire et efficace possible vers les employés qui ne feront qu'appliquer ce qu'on leur demande.
En cas de dysfonctionnement, on s'efforcera donc de cloisonner un peu plus le système, de réduire la marge de manoeuvre des employés pour éviter de "laisser la place à l'erreur humaine".

Sans forcément repasser par tous les travaux de [Frédéric Laloux](https://youtu.be/NZKqPoQiaDE) , [l'holacratie](https://fr.wikipedia.org/wiki/Holacratie) ou [la sociocratie](https://fr.wikipedia.org/wiki/Sociocratie), il est à mon avis important de constater que le modèle dominant cité plus haut, ne va pas tarder à être remplacé. Non pas parce qu'il ne fonctionne pas, il a parfaitement fonctionné pendant des années, mais parce qu'il n'est pas assez efficace face aux contraintes énergétiques de plus en plus fortes, et qu'il n'est pas assez flexible face à un monde qui évolue de plus en plus rapidement.

L'unité de production des ressources humaines est, vous l'avez deviné, l'humain. Malheureusement, à cause de [Mr Taylor](https://fr.wikipedia.org/wiki/Taylorisme), nous avons eu tendance à considérer l'humain comme une vulgaire pile, incapable de fonctionner ou de s'organiser sans un système de commande approprié pour synchroniser les énergies.

C'est cela qui est en train de changer.

D'une part, la circulation, le stockage et l'accès à l'information sont maintenant plus faciles, plus rapides et plus efficaces. L'intelligence moyenne des humains, pour peu qu'il apprenne à naviguer ces quantités énormes d'information, a donc gagné quelques points.

D'autre part, les techniques de facilitation en intelligence collective ont évolué rapidement sur les 20 (30 ? ☺ ) dernières années. Il ne s'agit plus de mettre les bonnes personnes dans les bonnes boites mais de concevoir des espaces, des cadres qui vont favoriser la productivité, l'innovation, la flexibilité et la collaboration.

Beaucoup d'entreprises ont déjà fait ce pas. Peu d'institutions les ont suivies. Mais c'est normal, parce qu'il manque une étape importante : la motivation.

Une entreprise fournit un salaire et nous avons été éduqués à penser que le fait de s'épanouir dans son travail est une chance qui n'est pas donnée à tout le monde, qu'il faut bien gagner sa vie et que nos parents ont souffert avant nous, etc.... Mais le salaire en lui-même suffit à se donner du mal.

Par contre, une institution ou un service public ne fournit pas de salaire. Au contraire, on lui reverse de l'argent tous les mois pour qu'il fonctionne.

Nous sommes contributeurs dans une entreprise et consommateurs le reste du temps pour la plupart (je ne nie absolument pas le travail énorme de tous les bénévoles dans toutes les associations du monde entier, j'ai même envie de leur faire un gros Big Up ici ☺ ). 

L'énergie potentielle se perd/dissipe/bloque à cause des doutes, des peurs, [de l'impuissance acquise](https://fr.wikipedia.org/wiki/Impuissance_apprise) et [des croyances limitantes](https://leblogdesrapportshumains.fr/top-10-des-croyances-limitantes-sur-soi-meme/) .

Le principe de Solucracy est donc de mettre le système au service de l'humain. Plutôt que l'énergie potentielle déployée soit frustrée, perdue, éliminée, il s'agit de la canaliser, de l'accompagner, de la laisser émerger.

![Image](../img/schema.png)


Le principe très important avec cette approche est l'économie d'énergie. On va utiliser ce qui existe localement et l'aider à grandir, l'accompagner, le canaliser, plutôt que d'essayer de pousser ou tirer des choses inertes, ou d'essayer d'organiser une multitude d'individus qui n'ont pas envie d'être organisés.

A l'aide d'incubateurs, de nouvelles entreprises ou associations vont pouvoir être créées et accompagnées pour assurer la durabilité des projets.

Les entreprises existantes ou associations auront également l'opportunité de sélectionner certains projets pour développer de nouveaux services ou produits.

Le principal défi étant que l'individu apprenne à dépasser le conditionnement de notre société, mis en place par un système éducatif développant la peur de l'erreur, des institutions étouffant toutes idées novatrices par leur complexité seule et un système économique prônant la compétition plutôt que la collaboration.

Axer la naissance des projets sur des besoins et des motivations réels devrait permettre mécaniquement de laisser de côté ceux qui ne trouveront ni public, ni contributeurs. 

Par exemple, lors des forums ouverts, si un projet ou une intiative ne trouve pas de contributeurs, il sera simplement laissé à l'abandon, pour concentrer les énergies sur le nécessaire, et oublier le superflu.

Vous me direz que tout cela ne sont que de belles théories, mais nous avons déjà pu tester celà à petite échelle.

Il n'y a plus qu'à reproduire et améliorer ☺ .
